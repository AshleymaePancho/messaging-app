import Routes from "./Routes";
import { UserContextProvider } from "./UserContext";
import axios from "axios"

function App() {
  // cookies!
  axios.defaults.baseURL = 'http://localhost:9000';
  axios.defaults.withCredentials = true;

  

  return (
    <>
      <UserContextProvider>
        <Routes />
      </UserContextProvider>
      
    </>
  )
}

export default App
